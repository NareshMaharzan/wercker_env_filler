chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    let data = request.data || {};
    let event = new Event('input', {
        bubbles: true,
        "cancelable": false
    });
    let click = new Event('click', {
        bubbles: true,
        "cancelable": false
    });

    let button = Array.from(document.querySelectorAll("button")).filter(el => el.textContent == 'Add');

    let tr = button[0].parentNode.parentNode.parentNode;
    let keyContainer = tr.querySelectorAll('.envvarItem_key');
    let valueContainer = tr.querySelectorAll('.envvarItem_value_container');

    keyContainer[0].querySelectorAll('input')[0].value = data.key;
    keyContainer[0].querySelectorAll('input')[0].dispatchEvent(event);

    valueContainer[0].querySelectorAll('textarea')[0].value = (data.value == "") ? " " : data.value;
    valueContainer[0].querySelectorAll('textarea')[0].dispatchEvent(event);

    button[0].dispatchEvent(click);
    sendResponse({ data: data, success: true });
});
