document.getElementById('fill').onclick = start;

function start() {
    fillForm().then(result => console.log(result));
}

function generateJsonValues() {
    const textAreaData = document.getElementById('env').value;
    return generatePropertyMapFromString(textAreaData);
}

function generatePropertyMapFromString(textAreaData) {
    let properties = {};
    const lines = textAreaData.split('\n');
    lines.forEach(element => {
        if (!(element.startsWith("#") || element.startsWith("\\") || element.trim() == "")) {
            let property = element.split(/^([^=]+)=/);
            let tmp = [];
            tmp[property[1]] = (property[2] == "") ? " " : property[2];
            properties = Object.assign(properties, tmp);
        }
    });
    return properties;
}

async function fillForm() {
    let env = generateJsonValues();
    for (v in env) {
        let mergedData = Object.assign({}, { "key": v, "value": env[v] });
        let wait = ms => new Promise((r, j) => setTimeout(r, ms));
        await wait(100);
        chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            chrome.tabs.sendMessage(tabs[0].id, { "data": mergedData }, function (response) {
            });
        });
    }
}